package Basketball;

public class Ball {
    private int Weight;
    private String Color;

    public Ball(String Color, int Weight) {
        this.Weight = Weight;
        this.Color = Color;
    }

    public String Get_Color() {
        return this.Color;
    }

    public int Get_Weight() {
        return this.Weight;
    }
}
