package Basketball;

import java.util.ArrayList;

public class Basket {

    private ArrayList<Ball> Total_Balls;

    public Basket() {
        Total_Balls = new ArrayList<>();
    }

    public ArrayList<Ball> getBalls() {
        return Total_Balls;
    }

    public void Add_Ball(String Color, int Weight) {
        Ball ball = new Ball(Color, Weight);
        Total_Balls.add(ball);
    }

    public int Get_Total_Balls_Weight() {
        int Basket_Weight = 0;
        for (Ball ball : Total_Balls) {
            Basket_Weight += ball.Get_Weight();
        }
        return Basket_Weight;
    }

    public int Get_Balls_Count_Color(String color) {
        int Balls_Count = 0;
        for (Ball ball : Total_Balls) {
            if (color.equals(ball.Get_Color())) {
                Balls_Count++;
            }
        }
        return Balls_Count;
    }
}
