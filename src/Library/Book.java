package Library;

import java.util.Objects;

public class Book implements Comparable<Book>{

    private String title;
    private String author;
    private int price;
    private static int edition;
    private int isbn;

    public Book(String title, String author, int price, int isbn) {
        this.title = title;
        this.author = author;
        this.price = price;
        this.isbn = isbn;
    }

    public int Get_Price() {
        return price;
    }

    public String Get_Author() {
        return author;
    }

    public String Get_Title() {
        return title;
    }

    @Override
    public boolean equals(Object otherBook) {
        if (otherBook == null) return false;
        if (getClass() != otherBook.getClass()) return false;
        return this == otherBook;
    }

    @Override
    public String toString() {
        return getClass().getName() + "[title=" + title
                + ", author=" + author + ", price=" + price + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, price);
    }

    public boolean CompareTo(Book book) {
        return this.isbn - book.isbn == 0;
    }

    public Object clone() {
        return new Book(this.title,this.author,this.price,this.isbn);
    }

    @Override
    public int compareTo(Book book) {
        return this.isbn - book.isbn;
    }
}
