package Library;

import java.util.Objects;

public class ProgrammerBook extends Book{

    private String language;
    private int level;

    public String Get_Language() {
        return language;
    }

    public int Get_Level() {
        return level;
    }

    public ProgrammerBook(String title, String author, int price, String language, int level, int isbn) {
        super(title, author, price, isbn);
        this.language = language;
        this.level = level;
    }

    @Override
    public boolean equals(Object otherBook) {
        return super.equals(otherBook);
    }

    @Override
    public String toString() {
        return super.toString() + "[language=" + language
                + ", level=" + level + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), language, level);
    }
}

