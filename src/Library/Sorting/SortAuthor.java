package Library.Sorting;

import Library.Book;

import java.util.Comparator;

public class SortAuthor implements Comparator<Book> {
    @Override
    public int compare(Book FrBook, Book SecBook) {
        return FrBook.Get_Author().compareToIgnoreCase(SecBook.Get_Author());
    }
}
