package Library.Sorting;

import Library.Book;

import java.util.Comparator;

public class SortAuthorTitle implements Comparator<Book>{
    private Comparator<Book> comparator;

    public SortAuthorTitle() {
        comparator = new SortAuthor().thenComparing(new SortTitle());
    }

    @Override
    public int compare(Book FrBook, Book SecBook) {
        return comparator.compare(FrBook, SecBook);
    }
}
