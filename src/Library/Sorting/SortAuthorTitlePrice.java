package Library.Sorting;

import Library.Book;

import java.util.Comparator;

public class SortAuthorTitlePrice implements Comparator<Book>{
    private Comparator<Book> comparator;

    public SortAuthorTitlePrice() {
        comparator = new SortAuthorTitle().thenComparing(new SortPrice());
    }

    @Override
    public int compare(Book FrBook, Book SecBook) {
        return comparator.compare(FrBook, SecBook);
    }

}
