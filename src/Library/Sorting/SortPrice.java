package Library.Sorting;

import Library.Book;

import java.util.Comparator;

public class SortPrice implements Comparator<Book>{
    @Override
    public int compare(Book FrBook, Book SecBook) {
        return FrBook.Get_Price() - SecBook.Get_Price();
    }
}
