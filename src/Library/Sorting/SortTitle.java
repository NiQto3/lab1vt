package Library.Sorting;

import Library.Book;

import java.util.Comparator;

public class SortTitle implements Comparator<Book> {
    @Override
    public int compare(Book FrBook, Book SecBook) {
        return FrBook.Get_Title().compareToIgnoreCase(SecBook.Get_Title());
    }
}
