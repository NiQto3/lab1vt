package Library.Sorting;

import Library.Book;

import java.util.Comparator;

public class SortTitleAuthor implements Comparator<Book> {
    private Comparator<Book> comparator;

    public SortTitleAuthor() {
        comparator = new SortTitle().thenComparing(new SortAuthor());
    }

    @Override
    public int compare(Book FrBook, Book SecBook) {
        return comparator.compare(FrBook, SecBook);
    }
}
