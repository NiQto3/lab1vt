import Basketball.Basket;

public class Main {
    public static void main(String[] args) {
//First Task
//        System.out.println(SolvingFirst(1,9));
//Second Task
//        System.out.println(SolvingSecond(1,4));
//Third task
//        SolvingThird(1,1,4);
//Forth task
//        int[] arr = {1,2,3,4,5,6,7};
//        System.out.println();
//        SolvingForth(arr);
//Fifth task
//        int[] arr2 = {11,12,3,14,5,6,18,7,9};
//        System.out.println();
//        SolvingFifth(arr2);
//Six task
//        int[] arr3 = {1,2,3,4,5,6,7,8,9};
//        System.out.println();
//        SolvingSix(arr3);
//Seven task
//        int[] arr4 = { 66, 23, 11, 45, 65, 88, 2, 37, 9 };
//        SolvingSeven(arr4);
//Eight task
//        int[] arr5a = {1,4,5,7};
//        int[] arr5b = {2,3,6,7,8,9};
//        System.out.println();
//        SolvingEight(arr5a, arr5b);
//9-11 task
//        Basket NBasket = new Basket();
//        NBasket.Add_Ball("RED", 12);
//        NBasket.Add_Ball("GREEN", 12);
//        NBasket.Add_Ball("BLUE", 13);
//        NBasket.Add_Ball("RED", 14);
//        String Color_Search = "RED";
//        System.out.println("Number of "+ Color_Search + " " + NBasket.Get_Balls_Count_Color(Color_Search));
//        System.out.println("Total weight:" + NBasket.Get_Total_Balls_Weight());
    }

    public static double SolvingFirst(double x, double y){
        double tmp;
        tmp = Math.pow(x,2)*Math.pow(y,2);
        tmp = 2 + Math.abs(x-(2*x/(1+tmp)));
        tmp = (1 + Math.pow(Math.sin(x+y), 2))/tmp + x;
        return tmp;
    }

    public static boolean SolvingSecond(int x, int y){
        return (Math.abs(y) <= 5 && Math.abs(x) <= 4) || (Math.abs(y) <= 3 && Math.abs(x) <= 6);
    }

    public static void SolvingThird(int h, int a, int b){
        for (int i = a; i<=b; i+=h){
            System.out.print(i);
            System.out.printf(" | %.2f", Math.tan(i));
            System.out.println();
        }
    }

    public static boolean IsPrime(int num){
        for (int i = 2; i <= num / 2; ++i) {
            // condition for nonprime number
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void SolvingForth(int[] N){
        for (int i = 0; i< N.length; i++){
            if (IsPrime(N[i])){
                System.out.println(i);
            }
        }
    }

    public static void SolvingFifth(int[] Nums){
        int i = 0;
        int[] NDel = new int[Nums.length];
        int pos = 0;
        int TDel;
        int j;
        while (i < Nums.length){
            TDel = 0;
            j = i;
            for(int b = i + 1; b<Nums.length; b++){
                if (Nums[b] < Nums[j]){
                    TDel++;
                }else j = b;
            }
            j = i;
            if (i>0){
                for (int b = i-1; b >= 0; b--){
                    if (Nums[b] > Nums[j]){
                        TDel++;
                    }else j = b;
                }
            }
            NDel[pos] = TDel;
            pos++;
            i++;
        }
        int min = 1000;
        for (int a:NDel){
            if (NDel[a]<min) min = NDel[a];
        }
        System.out.print("min is = ");
        System.out.println(min);
    }

    public static void SolvingSix(int[] Nume){
        int[][] NNew = new int[Nume.length][Nume.length];
        int ln = Nume.length;
        for (int i = 0; i < ln; i++){
            for(int j = 0; j < ln; j++){
                NNew[i][j] = Nume[(i+j) % ln];
                System.out.print(NNew[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void SolvingSeven(int[] mass){//its Shell not bubble
        int i, j, step, tmp;
        int n = mass.length;
        for (step = n / 2; step > 0; step /= 2){
            for (i = step; i < n; i++)
            {
                tmp = mass[i];
                for (j = i; j >= step; j -= step)
                {
                    if (tmp < mass[j - step])
                        mass[j] = mass[j - step];
                    else
                        break;
                }
                mass[j] = tmp;
            }
        }
        for (int k : mass) System.out.printf("%d ", k);
    }

    public static void SolvingEight(int[] A, int[] B){
        int j = 0;
        for (int i = 0; i+1 < A.length; i++){
            while (B[j]>A[i] && B[j]<A[i+1]){
                System.out.printf("%d ", i);
                j++;
            }
        }
        for (int i = j; i < B.length; i++){
            System.out.printf("%d ", A.length);
        }
    }
}

